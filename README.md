# C++ samples

Examples to clean, compile, link and run C++ apps on Linux or Docker, using:
- ``g++`` and ``make``
- Linux host
- Linux host + Docker (Alpine Linux).
- Windows host + Docker (Alpine Linux).

Includes examples of:
- Dockerfile
- Makefile
- shell script (Linux)
- batch script (Windows)


## Table of Contents

[[_TOC_]]


## Using on Linux

It requires ``git``, ``g++`` and ``make`` (see [Dockerfile](Dockerfile)).

On Linux terminal:

```
git clone https://gitlab.com/jorge.gajardo/cpp-samples.git

cd cpp-samples

# Enter to a 'src/sample' folder and run 'make' command. For example:

cd src/functions

make all

./bin/area
./bin/area square 5
```


## Using with Docker on Linux host

On Linux terminal:

```
git clone https://gitlab.com/jorge.gajardo/cpp-samples.git

cd cpp-samples/docker/linux

sh ./1.image-builder.sh

sh ./2.container-builder.sh

sh ./3.container-sh.sh
```

On container, enter to a ``src/sample`` folder and run ``make`` command. For example:

```
cd src/functions

make all

./bin/area
./bin/area rectangle 4 5
```


## Using with Docker on Windows host

On Windows cmd:

```
git clone https://gitlab.com/jorge.gajardo/cpp-samples.git

cd cpp-samples\docker\windows

1.image-builder.bat

2.container-builder.bat

3.container-sh.bat
```

On container, enter to a ``src/sample`` folder and run ``make`` command. For example:

```
cd src/functions

make all

./bin/area
./bin/area rectangle 4 5
```

## Make command examples

```
cd src/functions

# Clean binary files (remove ./obj and ./bin folders)
make clean

# Compile and link (either one can be used):
make
make all

# Clean, compile and link (either one can be used):
make full
make clean all
```


## Detail of the examples

- [``src/functions``](src/functions):
  - ``area.cpp``: function basics
    - Functions to calculate the area of geometric shapes.
    - Use basic CLI params.
    - Passing parameters to a function by value, pointer and reference.
    - Example of using directives: ``#include``, ``#ifndef/#endif`` and ``#define``.
  - ``makefile``: sample with ``g++`` to clean, compile, link and run.
- ``docker``
  - [linux](docker/linux): shell files to create Docker image and container to clean, compile, link and run C++ apps.
    - ``0.setenv.sh``: set environment variables.
    - ``1.image-builder.sh``: create Docker image.
    - ``2.container-builder.sh``: create Docker container.
    - ``3.bash-container.sh``: enter to ``sh`` terminal in Docker container.
  - [windows](docker/windows): batch files to create Docker image and container to clean, compile, link and run C++ apps.
    - ``0.setenv.bat``: set environment variables.
    - ``1.image-builder.bat``: create Docker image.
    - ``2.container-builder.bat``: create Docker container.
    - ``3.bash-container.bat``: enter to ``sh`` terminal in Docker container.
  - Highlights
    - Shells and batches can be executed with absolute or relative path.
    - They detect in which path they are installed.
    - They use environment variables to configure names of:
      - Dockerfile.
      - Docker image
      - Docker container.
    - These variables are configured in a single file, which is used by the others.
    - The environment variables used are not available on the host after the shell/batch files finish running, nor on the Docker image.

## References

- [Introduction to OOP](https://sbme-tutorials.github.io/2019/stl-intro/notes/1_oop1.html)
- [Pointers vs References in C++](https://www.geeksforgeeks.org/pointers-vs-references-cpp)
- [Passing By Pointer Vs Passing By Reference in C++](https://www.geeksforgeeks.org/passing-by-pointer-vs-passing-by-reference-in-c)
- https://github.com/sinairv/Cpp-Tutorial-Samples
- [Learn Makefiles](https://makefiletutorial.com)
