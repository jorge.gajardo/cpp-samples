#include <iostream> 
#include <math.h>
#include "area.hpp"

// See:
// https://sbme-tutorials.github.io/2019/stl-intro/notes/1_oop1.html
// https://www.geeksforgeeks.org/pointers-vs-references-cpp/
// https://www.geeksforgeeks.org/passing-by-pointer-vs-passing-by-reference-in-c
// https://github.com/sinairv/Cpp-Tutorial-Samples

void print_use(char* program)
{
    std::cout << "Use:\n";
    std::cout << program << " square side\n";
    std::cout << program << " rectangle width height\n";
    std::cout << program << " circle radius\n";
    std::cout << program << " triangle base height\n";
    std::cout << program << " byvalue side\n";
}

int main(int argc, char **argv)
{
    if (argc == 1) {
        std::cout << "Error: missing params\n";
        print_use(argv[0]);
        exit(EXIT_FAILURE);
    }

    std::string shape = argv[1];
    double area = 0;
    
    if (shape == "square")
    {
        double side = std::atof(argv[2]);
        area = geometry::squareArea(side);
    }
    else if (shape == "rectangle")
    {
        double width = std::atof(argv[2]);
        double height = std::atof(argv[3]);
        area = geometry::rectangleArea(width, height);
    }
    else if (shape == "circle")
    {
        double radius = std::atof(argv[2]);
        geometry::circleArea(radius, &area);
    }
    else if (shape == "triangle")
    {
        double a = std::atof(argv[2]);
        double b = std::atof(argv[3]);
        geometry::triangleArea(a, b, area);
    }
    else if (shape == "byvalue")
    {
        double side = std::atof(argv[2]);
        geometry::byValueArea(side, area);
    }
    else
    {
        std::cout << "Undefined shape! " << shape << "\n";
        exit(EXIT_FAILURE);
    }

    std::cout << "area: " << area << std::endl;

    return 0;
}