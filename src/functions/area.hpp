#ifndef GEOMETRY_HPP
#define GEOMETRY_HPP

namespace geometry
{
    double squareArea(double side)
    {
        return side * side;
    }

    double rectangleArea(double width, double height)
    {
        return width * height;
    }

    // Passing parameters by pointer
    void circleArea(double radius, double *area)
    {
        *area = M_PI * radius * radius;
    }

    // Passing parameters by reference
    void triangleArea(double base, double height, double &area)
    {
        area = base * height / 2;
    }

    // Passing parameters by value, area 
    // the value of the area parameter changes only locally
    void byValueArea(double side, double area)
    {
        area = side * side;
    }
}

#endif