FROM alpine:3.15.4

# See
# https://github.com/prantlf/docker-alpine-make-gcc

### Install compiler tools and git:
RUN apk --no-cache add make gcc g++ musl-dev binutils autoconf automake libtool pkgconfig check-dev file patch git
### :Install compiler tools

### Download project to image:
RUN mkdir /src \
  && cd /src \
  && git clone https://gitlab.com/jorge.gajardo/cpp-samples.git
### :Download project to image

WORKDIR /src/cpp-samples
