# Command line instructions
Previous:
- Create empty project in GitLab without Initialize repository without a README

You can also upload existing files from your computer using the instructions below.

## Git global setup
```
git config --global user.name "Name"
git config --global user.email "email"
```

## Create a new repository
```
git clone https://gitlab.com/jorge.gajardo/cpp-samples.git
cd cpp-samples
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main
```

## Push an existing folder
```
cd cpp-samples
git init --initial-branch=main
git remote add origin https://gitlab.com/jorge.gajardo/cpp-samples.git
git add .
git commit -m "First commit"
git push -u origin main
```

## Push an existing Git repository
```
cd cpp-samples
git remote rename origin old-origin
git remote add origin https://gitlab.com/jorge.gajardo/cpp-samples.git
git push -u origin --all
git push -u origin --tags
```