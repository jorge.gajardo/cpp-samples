SCRIPT_PATH=`realpath $0`
SCRIPT_PATH=`dirname $SCRIPT_PATH`
cd $SCRIPT_PATH

source ./0.setenv.sh

echo
echo [INFO] Running bash on C++ sample container
echo [INFO] ====================================
echo [INFO] Container: $CONTAINER_NAME
echo

echo docker exec -it $CONTAINER_NAME /bin/sh

docker exec -it $CONTAINER_NAME /bin/sh
