SCRIPT_PATH=`realpath $0`
SCRIPT_PATH=`dirname $SCRIPT_PATH`
cd $SCRIPT_PATH

source ./0.setenv.sh

cd ../..

echo
echo [INFO] Building Docker image for C++ samples
echo [INFO] =====================================
echo [INFO] Image:      $IMAGE_TAG
echo [INFO] Dockerfile: $DOCKER_FILE
echo

echo docker image build --file $DOCKER_FILE --tag $IMAGE_TAG .

docker image build --file $DOCKER_FILE --tag $IMAGE_TAG .
