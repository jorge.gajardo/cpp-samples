SCRIPT_PATH=`realpath $0`
SCRIPT_PATH=`dirname $SCRIPT_PATH`
cd $SCRIPT_PATH

source ./0.setenv.sh

echo
echo [INFO] Building Docker container for C++ samples
echo [INFO] =========================================
echo [INFO] Container: $CONTAINER_NAME
echo [INFO] Image:     $IMAGE_TAG
echo

echo docker run --name $CONTAINER_NAME --detach --tty $IMAGE_TAG

docker run --name $CONTAINER_NAME --detach --tty $IMAGE_TAG
