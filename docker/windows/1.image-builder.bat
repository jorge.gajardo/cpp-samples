@echo off
setlocal enabledelayedexpansion

cd /d "%~dp0"

call 0.setenv.bat

cd ..\..

echo.
echo [INFO] Building Docker image for C++ samples
echo [INFO] =====================================
echo [INFO] Image:      %IMAGE_TAG%
echo [INFO] Dockerfile: %DOCKER_FILE%
echo.

echo docker image build --file %DOCKER_FILE% --tag %IMAGE_TAG% .

docker image build --file %DOCKER_FILE% --tag %IMAGE_TAG% .
