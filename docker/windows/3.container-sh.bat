@echo off

setlocal enabledelayedexpansion

cd /d "%~dp0"

call 0.setenv.bat

echo.
echo [INFO] Running bash on C++ sample container
echo [INFO] ====================================
echo [INFO] Container: %CONTAINER_NAME%
echo.

echo docker exec -it %CONTAINER_NAME% /bin/sh

docker exec -it %CONTAINER_NAME% /bin/sh
