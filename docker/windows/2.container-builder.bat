@echo off

setlocal enabledelayedexpansion

cd /d "%~dp0"

call 0.setenv.bat

echo.
echo [INFO] Building Docker container for C++ samples
echo [INFO] =========================================
echo [INFO] Container: %CONTAINER_NAME%
echo [INFO] Image:     %IMAGE_TAG%
echo.

echo docker run --name %CONTAINER_NAME% --detach --tty %IMAGE_TAG%

docker run --name %CONTAINER_NAME% --detach --tty %IMAGE_TAG%
